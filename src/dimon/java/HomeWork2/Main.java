package dimon.java.HomeWork2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String[] words = {"tree", "tru"};
        Scanner in = new Scanner(System.in);
        System.out.println("Enter strings count:");
        int n = in.nextInt();
        String[] strings = new String[n];

        for(int i=0; i<n; i++){
            System.out.println("Enter new string:");
            strings[i]=in.next();
        }

        boolean isContain = false;
        for (String item : strings) {
            int trueWords = 0;
            int arrayElementCount =0;//Не нашел, как с помощью функций найти количество элементов массива
            for (String word : words) {
                //System.out.println(item+" "+word);
                arrayElementCount+=1;
                if (item.contains(word)) {
                    trueWords+=1;
                }
            }
            if(trueWords==arrayElementCount){
                System.out.println(item);
            }
        }
    }
}
